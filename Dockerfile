FROM golang:1.12

WORKDIR /go/src/gitlab.com/tokend/tron-svc

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o /usr/local/bin/tron-svc gitlab.com/tokend/tron-svc


###

FROM alpine:3.9

COPY --from=0 /usr/local/bin/tron-svc /usr/local/bin/tron-svc
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["tron-svc"]
