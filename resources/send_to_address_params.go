package resources

type SendToAddressParams struct {
	Currency string
	Amount   float64
	Address  string
}
