module gitlab.com/tokend/tron-svc

go 1.16

require (
	github.com/Masterminds/squirrel v1.5.0
	github.com/alecthomas/kingpin v2.2.6+incompatible
	github.com/alecthomas/units v0.0.0-20210208195552-ff826a37aa15 // indirect
	github.com/btcsuite/btcd v0.21.0-beta
	github.com/btcsuite/btcutil v1.0.3-0.20201208143702-a53e38424cce
	github.com/certifi/gocertifi v0.0.0-20210429200110-83314bf6d27c // indirect
	github.com/deliveroo/jsonrpc-go v1.0.0
	github.com/ethereum/go-ethereum v1.10.1
	github.com/fatih/structs v1.1.0
	github.com/fbsobreira/gotron-sdk v0.0.0-20210409100018-9213b0ef130f
	github.com/getsentry/raven-go v0.2.0 // indirect
	github.com/getsentry/sentry-go v0.11.0 // indirect
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible // indirect
	github.com/gobuffalo/logger v1.0.4 // indirect
	github.com/gobuffalo/packr/v2 v2.8.1
	github.com/google/jsonapi v1.0.0 // indirect
	github.com/jmoiron/sqlx v1.3.4 // indirect
	github.com/karrick/godirwalk v1.16.1 // indirect
	github.com/lib/pq v1.10.2 // indirect
	github.com/magiconair/properties v1.8.5 // indirect
	github.com/miguelmota/go-ethereum-hdwallet v0.0.1 // indirect
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	github.com/pelletier/go-toml v1.9.1 // indirect
	github.com/rubenv/sql-migrate v0.0.0-20210408115534-a32ed26c37ea
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/spf13/afero v1.6.0 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/viper v1.7.1 // indirect
	github.com/tyler-smith/go-bip39 v1.1.0
	gitlab.com/distributed_lab/ape v1.5.0 // indirect
	gitlab.com/distributed_lab/figure v2.1.0+incompatible
	gitlab.com/distributed_lab/kit v1.8.5
	gitlab.com/distributed_lab/logan v3.8.0+incompatible
	gitlab.com/distributed_lab/lorem v0.2.0 // indirect
	gitlab.com/distributed_lab/running v1.6.0 // indirect
	golang.org/x/sys v0.0.0-20210531080801-fdfd190a6549 // indirect
	golang.org/x/term v0.0.0-20210503060354-a79de5458b56 // indirect
	golang.org/x/text v0.3.6 // indirect
	gopkg.in/ini.v1 v1.62.0 // indirect
)
