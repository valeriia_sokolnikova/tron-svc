package pg

import (
	"database/sql"
	sq "github.com/Masterminds/squirrel"
	"github.com/fatih/structs"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/tokend/tron-svc/internal/data"
)

const addressTableName = "addresses"

func NewAddressQ(db *pgdb.DB) data.AddressQ {
	return &addressQ{
		db:  db,
		sql: sq.StatementBuilder,
	}
}

type addressQ struct {
	db  *pgdb.DB
	sql sq.StatementBuilderType
}


func (q *addressQ) Get() (*data.Address, error) {
	var result data.Address
	stmt := q.sql.Select("*").From(addressTableName)
	err := q.db.Get(&result, stmt)
	if err == sql.ErrNoRows {
		return nil, err
	}
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (q *addressQ) Select() ([]data.Address, error) {
	var result []data.Address
	stmt := q.sql.Select("*").From(addressTableName)
	err := q.db.Select(&result, stmt)
	if err == sql.ErrNoRows {
		return nil, err
	}
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (q *addressQ) Insert(value data.Address) (data.Address, error) {
	clauses := structs.Map(value)

	var result data.Address
	stmt := sq.Insert(addressTableName).SetMap(clauses).Suffix("returning *")
	err := q.db.Get(&result, stmt)
	if err != nil {
		return data.Address{}, err
	}
	return result, nil
}

func (q *addressQ) GetLastIndex() (int, error) {
	var result data.Address
	stmt := q.sql.Select("*").From(addressTableName).OrderBy("index desc").Limit(1)
	err := q.db.Get(&result, stmt)
	if err == sql.ErrNoRows {
		return 0, err
	}
	if err != nil {
		return 0, err
	}
	return result.Index, nil
}
