package pg

import (
	"database/sql"
	sq "github.com/Masterminds/squirrel"
	"github.com/fatih/structs"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/tokend/tron-svc/internal/data"
)

const depositesTableName = "deposits"

func NewDepositeQ(db *pgdb.DB) data.DepositQ {
	return &depositQ{
		db:  db,
		sql: sq.StatementBuilder,
	}
}

type depositQ struct {
	db  *pgdb.DB
	sql sq.StatementBuilderType
}


func (q *depositQ) Get() (*data.Deposit, error) {
	var result data.Deposit
	stmt := q.sql.Select("*").From(depositesTableName)
	err := q.db.Get(&result, stmt)
	if err == sql.ErrNoRows {
		return nil, err
	}
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (q *depositQ) Select() ([]data.Deposit, error) {
	var result []data.Deposit
	stmt := q.sql.Select("*").From(depositesTableName)
	err := q.db.Select(&result, stmt)
	if err == sql.ErrNoRows {
		return nil, err
	}
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (q *depositQ) Insert(value data.Deposit) (data.Deposit, error) {
	clauses := structs.Map(value)

	var result data.Deposit
	stmt := sq.Insert(depositesTableName).SetMap(clauses).Suffix("returning *")
	err := q.db.Get(&result, stmt)
	if err != nil {
		return data.Deposit{}, err
	}
	return result, nil
}
