package data

type DepositQ interface {
	Get() (*Deposit, error)
	Select() ([]Deposit, error)
	Insert(value Deposit) (Deposit, error)
}

type Deposit struct {
	ID            string  `db:"id" structs:"-"`
	Amount        float64 `db:"amount" struct:"amount"`
	Confirmations int     `db:"confirmations" structs:"confirmations"`
	Blockhash     string  `db:"blockhash" structs:"blockhash"`
	Blockheight   int64   `db:"blockheight" structs:"blockheight"`
	Blockindex     int64  `db:"blockindex" structs:"blockindex"`
	Blocktime     int64  `db:"blocktime" structs:"blocktime"`
	TxId     string  `db:"txid" structs:"txid"`
	Time     int64  `db:"time" structs:"time"`
	Timereceived     int64  `db:"timereceived" structs:"timereceived"`
	Bip125Replaceable     string  `db:"bip125replaceable" structs:"bip125replaceable"`
	Details DepositDetails `db:"details" structs:"details"`
	Hex string  `db:"hex" structs:"hex"`
}

type DepositDetails struct {
	ID       string  `db:"id" struct:"-"`
	Address  string  `db:"address" struct:"address"`
	Category string  `db:"category" struct:"category"`
	Amount   float64 `db:"amount" struct:"amount"`
	Label    string  `db:"label" struct:"label"`
	Vout     int64   `db:"vout" struct:"vout"`
}
