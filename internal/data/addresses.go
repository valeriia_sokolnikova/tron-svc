package data

type AddressQ interface {
	Get() (*Address, error)
	Select() ([]Address, error)
	Insert(value Address) (Address, error)

	GetLastIndex() (int, error)
}

type Address struct {
	ID      string  `db:"id" structs:"-"`
	Index   int     `db:"index" struct:"index"`
	Address string  `db:"address" structs:"address"`
	Balance float64 `db:"balance" structs:"balance"`
}
