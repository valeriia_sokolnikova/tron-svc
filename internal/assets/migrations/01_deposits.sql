-- +migrate Up

CREATE TABLE deposits_details
(
    id          BIGSERIAL PRIMARY KEY,
    address     TEXT NOT NULL,
    category    TEXT NOT NULL,
    amount      FLOAT NOT NULL,
    label       TEXT NOT NULL,
    vout        BIGINT NOT NULL
);
CREATE TABLE deposits
(
    id                  BIGSERIAL PRIMARY KEY,
    amount              FLOAT NOT NULL,
    confirmations       SMALLINT NOT NULL,
    blockhash           TEXT NOT NULL,
    blockheight         BIGINT NOT NULL,
    blockindex          BIGINT NOT NULL,
    blocktime           BIGINT NOT NULL,
    txid                TEXT NOT NULL,
    time                BIGINT NOT NULL,
    timereceived        BIGINT NOT NULL,
    bip125replaceable   TEXT NOT NULL,
    details             BIGINT REFERENCES deposits_details(id),
    hex                 TEXT NOT NULL
);

-- +migrate Down

DROP TABLE deposits;
DROP TABLE deposits_details;