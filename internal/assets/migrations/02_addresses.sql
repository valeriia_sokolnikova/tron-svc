-- +migrate Up

CREATE TABLE addresses
(
    id      BIGSERIAL PRIMARY KEY,
    index   INT NOT NULL,
    address TEXT UNIQUE NOT NULL,
    balance FLOAT       NOT NULL
);

-- +migrate Down

DROP TABLE addresses;