package address_generator

import (
	"fmt"
	"log"

	"github.com/fbsobreira/gotron-sdk/pkg/address"
	"github.com/fbsobreira/gotron-sdk/pkg/keys"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/tron-svc/internal/config"
	"gitlab.com/tokend/tron-svc/internal/data"
	"gitlab.com/tokend/tron-svc/internal/data/pg"
)

type Service struct {
	addressQ  data.AddressQ
	generator config.GeneratorConfig
}

func New(cfg config.Config) *Service {
	return &Service{
		addressQ:  pg.NewAddressQ(cfg.DB()),
		generator: cfg.GeneratorConfig(),
	}
}

func (s *Service) Run() error {
	lastIndex, err := s.addressQ.GetLastIndex()
	if err != nil {
		lastIndex = -1
	}
	index := s.generator.Index
	mnemonic := s.generator.Mnemonic
	for i := lastIndex + 1; i <= lastIndex+index; i++ {
		privateKey, _ := keys.FromMnemonicSeedAndPassphrase(mnemonic, "", i)
		newAddress := address.PubkeyToAddress(privateKey.PublicKey)
		address := data.Address{Address: newAddress.String(), Balance: 0.0, Index: i}
		_, err = s.addressQ.Insert(address)
		if err != nil {
			log.Print(errors.Wrap(err, "Failed to insert address"))
			return errors.Wrap(err, "Failed to insert address")
		}
	}
	log.Print(fmt.Sprintf("%d addresses created", index))
	return nil
}
