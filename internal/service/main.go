package service

import (
    "gitlab.com/distributed_lab/kit/copus/types"
    "gitlab.com/distributed_lab/logan/v3"
    "gitlab.com/tokend/tron-svc/internal/config"
    "net"
)

type service struct {
    log      *logan.Entry
    copus    types.Copus
    listener net.Listener
}

func (s *service) run() error {
    // TODO implement custom logic here
    return nil
}

func newService(cfg config.Config) *service {
    return &service{
        log:        cfg.Log(),
        copus:      cfg.Copus(),
        listener:   cfg.Listener(),
    }
}

func Run(cfg config.Config) {
    if err := newService(cfg).run(); err != nil {
        panic(err)
    }
}
