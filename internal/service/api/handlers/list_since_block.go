package handlers

import (
	"context"

	"gitlab.com/tokend/tron-svc/internal/service/api/requests"
	"gitlab.com/tokend/tron-svc/resources"
)

func ListSinceBlock(ctx context.Context, params resources.Params) (interface{}, error) {
	blockNumber, err := requests.ListSinceBlock(params)
	if err != nil {
		return nil, err
	}

	// FIXME: Add logic

	return blockNumber, nil
}
