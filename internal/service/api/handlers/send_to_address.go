package handlers

import (
	"context"

	"gitlab.com/tokend/tron-svc/internal/service/api/requests"
	"gitlab.com/tokend/tron-svc/resources"
)

func SendToAddress(ctx context.Context, params resources.Params) (interface{}, error) {
	request, err := requests.SendToAddress(params)
	if err != nil {
		return nil, err
	}

	// FIXME: Add logic

	return request, nil
}
