package handlers

import (
	"context"

	"gitlab.com/tokend/tron-svc/internal/service/api/requests"
	"gitlab.com/tokend/tron-svc/resources"
)

func GetTransaction(ctx context.Context, params resources.Params) (interface{}, error) {
	txId, err := requests.GetTransaction(params)
	if err != nil {
		return nil, err
	}

	// FIXME: Add logic

	return txId, nil
}
