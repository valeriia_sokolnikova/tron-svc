package handlers

import (
	"context"

	"gitlab.com/tokend/tron-svc/internal/service/api/requests"
	"gitlab.com/tokend/tron-svc/resources"
)

func GetNewAddress(ctx context.Context, params resources.Params) (interface{}, error) {
	currency, err := requests.GetCurrencyParam(params)
	if err != nil {
		return nil, err
	}

	// FIXME: Add logic

	return currency, nil
}
