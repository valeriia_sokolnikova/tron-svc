package requests

import (
	"github.com/deliveroo/jsonrpc-go"
	"gitlab.com/tokend/tron-svc/resources"
)

func SendToAddress(params resources.Params) (resources.SendToAddressParams, error) {
	var request resources.SendToAddressParams
	if len(params) != 3 {
		return request, jsonrpc.InvalidParams("method expects 3 params")
	}

	var ok bool
	request.Currency, ok = params[0].(string)
	if !ok {
		return request, jsonrpc.InvalidParams("currency param should be string")
	}

	request.Amount, ok = params[0].(float64)
	if !ok {
		return request, jsonrpc.InvalidParams("amount param should be float")
	}

	request.Address, ok = params[0].(string)
	if !ok {
		return request, jsonrpc.InvalidParams("address param should be string")
	}

	return request, nil
}
