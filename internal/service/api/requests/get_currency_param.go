package requests

import (
	"github.com/deliveroo/jsonrpc-go"
	"gitlab.com/tokend/tron-svc/resources"
)

func GetCurrencyParam(params resources.Params) (string, error) {
	if len(params) != 1 {
		return "", jsonrpc.InvalidParams("method expects only one param")
	}
	currency, ok := params[0].(string)
	if !ok {
		return "", jsonrpc.InvalidParams("currency should be string")
	}

	return currency, nil
}
