package requests

import (
	"github.com/deliveroo/jsonrpc-go"
	"gitlab.com/tokend/tron-svc/resources"
)

func GetTransaction(params resources.Params) (string, error) {
	if len(params) != 1 {
		return "", jsonrpc.InvalidParams("method expects only one param")
	}
	txId, ok := params[0].(string)
	if !ok {
		return "", jsonrpc.InvalidParams("transaction id should be string")
	}

	return txId, nil
}
