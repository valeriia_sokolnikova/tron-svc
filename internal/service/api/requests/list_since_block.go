package requests

import (
	"github.com/deliveroo/jsonrpc-go"
	"gitlab.com/tokend/tron-svc/resources"
)

func ListSinceBlock(params resources.Params) (int64, error) {
	if len(params) != 1 {
		return 0, jsonrpc.InvalidParams("method expects only one param")
	}
	fBlockNumber, ok := params[0].(float64)
	blockNumber := int64(fBlockNumber)
	if !ok {
		return 0, jsonrpc.InvalidParams("block number should be uint64")
	}

	return blockNumber, nil
}
