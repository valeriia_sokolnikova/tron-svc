package api

import (
	"net/http"

	"gitlab.com/tokend/tron-svc/internal/service/api/handlers"

	"github.com/deliveroo/jsonrpc-go"
	"gitlab.com/tokend/tron-svc/internal/config"
)

func NewRouter(cfg config.Config) http.Handler {
	server := jsonrpc.New()

	server.Use(
		LoggingMiddleware(cfg.Log()),
		CtxMiddleware(
			handlers.CtxLog(cfg.Log()),
		),
	)

	server.Register(jsonrpc.Methods{
		"getbalance":     handlers.GetBalance,
		"gettransaction": handlers.GetTransaction,
		"getnewaddress":  handlers.GetNewAddress,
		"sendtoaddress":  handlers.SendToAddress,
		"listsinceblock": handlers.ListSinceBlock,
	})

	return NewBasicAuthRpc(server, "jsonrpc", cfg.Credentials())
}
