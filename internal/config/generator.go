package config

import (
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

type GeneratorConfig struct {
	Index    int    `fig:"index,required"`
	Mnemonic string `fig:"mnemonic,required"`
}

func (c *config) GeneratorConfig() GeneratorConfig {
	c.once.Do(func() interface{} {
		var config GeneratorConfig

		err := figure.
			Out(&config).
			With(figure.BaseHooks).
			From(kv.MustGetStringMap(c.getter, "generator")).
			Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to figure out info generator"))
		}

		c.generator = config
		return nil
	})

	return c.generator
}
