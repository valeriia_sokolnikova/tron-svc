package config

import (
	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/kv"
)

type BasicAuther interface {
	Credentials() map[string]string
}

func NewBasicAuther(getter kv.Getter) BasicAuther {
	return &basicAuther{
		getter: getter,
	}
}

type basicAuther struct {
	getter kv.Getter
	once   comfig.Once
}

func (d *basicAuther) Credentials() map[string]string {
	return d.once.Do(func() interface{} {
		raw, err := d.getter.GetStringMap("credentials")
		if err != nil {
			raw = make(map[string]interface{})
		}

		creds := make(map[string]string)
		for user, pass := range raw {
			creds[user] = pass.(string)
		}

		return creds
	}).(map[string]string)
}
